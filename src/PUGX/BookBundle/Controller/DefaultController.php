<?php

namespace PUGX\BookBundle\Controller;

use PUGX\BookBundle\Event\BookCreationEvent;
use Symfony\Component\HttpFoundation\Request;
use PUGX\BookBundle\Entity\Book;
use PUGX\BookBundle\Form\Type\BookType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('PUGXBookBundle:Default:index.html.twig');
    }

    public function booksAction()
    {
        $em    = $this->getDoctrine()->getManager();
        $books = $em->getRepository('PUGXBookBundle:Book')->findAllWithAuthors();

        return $this->render('PUGXBookBundle:Default:books.html.twig', array('books' => $books));
    }

    public function bookDetailAction($bookId)
    {
        $em   = $this->getDoctrine()->getManager();
        $book = $em->getRepository('PUGXBookBundle:Book')->findOneById($bookId);

        if (!$book) {
            throw $this->createNotFoundException('Book not found');
        }

        return $this->render('PUGXBookBundle:Default:bookDetail.html.twig', array('book' => $book));
    }

    public function bookCreateAction(Request $request)
    {
        $book = new Book();

        $form = $this->createForm(new BookType(), $book);

        if ($request->isMethod('POST')) {
            $form->bind($request);

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($book);
                $em->flush();

                //$this->get('pugx_book.mailer')->sendNewBookNotification($book);
                $dispatcher = $this->container->get('event_dispatcher');
                $event      = new BookCreationEvent($book);

                $dispatcher->dispatch('pugx.book-creation', $event);

                $this->get('session')->getFlashBag()->add('notice', 'Book successfully created');

                return $this->redirect($this->generateUrl('pugx_book_list'));
            }
        }

        return $this->render('PUGXBookBundle:Default:bookCreate.html.twig', array('form' => $form->createView()));
    }

    public function bookDeleteAction($bookId)
    {
        $em   = $this->getDoctrine()->getManager();
        $book = $em->getRepository('PUGXBookBundle:Book')->findOneById($bookId);

        if (!$book) {
            throw $this->createNotFoundException('Book not found');
        }

        $em->remove($book);
        $em->flush();

        $this->get('session')->getFlashBag()->add('notice', 'Book successfully deleted');

        return $this->redirect($this->generateUrl('pugx_book_list'));
    }

}
