<?php

namespace PUGX\BookBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use PUGX\BookBundle\Entity\Author;

class LoadAuthorData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $author = new Author();
        $author->setName("Kent");
        $author->setSurname("Beck");
        $author->setBio("Kent Beck is an American software engineer and the creator of the Extreme Programming and Test Driven Development software development methodologies, also named agile software development.");
        $manager->persist($author);
        $this->addReference('author-beck', $author);

        $author = new Author();
        $author->setName("Robert C.");
        $author->setSurname("Martin");
        $author->setBio("Robert Cecil Martin, known colloquially as 'Uncle Bob', is an American software consultant and author. Martin has been a software professional since 1970 and an international software consultant since 1990.");
        $manager->persist($author);
        $this->addReference('author-martin', $author);

        $author = new Author();
        $author->setName("Eric");
        $author->setSurname("Evans");
        $author->setBio("Eric Evans is a specialist in domain modeling and design in large business systems.");
        $manager->persist($author);
        $this->addReference('author-evans', $author);

        $manager->flush();
    }
   
    
    public function getOrder()
    {
        return 1;
    }
}