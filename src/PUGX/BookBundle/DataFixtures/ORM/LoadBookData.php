<?php

namespace PUGX\BookBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use PUGX\BookBundle\Entity\Book;

class LoadBookData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $books = array(
            new Book(
                "Extreme Programming Explained: Embrace Change",
                $this->getReference('author-beck'),
                new \DateTime('1999-10-5')
            ),
            new Book(
                "The Clean Coder",
                $this->getReference('author-martin'),
                new \DateTime('2011-5-23')
            ),
            new Book(
                "Domain-Driven Design: Tackling Complexity in the Heart of Software",
                $this->getReference('author-evans'),
                new \DateTime('2003-8-30')
            ),
        );

        foreach ($books as $book) {
            $manager->persist($book);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 2;
    }
}