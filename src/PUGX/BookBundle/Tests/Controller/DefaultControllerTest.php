<?php

namespace PUGX\BookBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use PUGX\BookBundle\Entity\Book;

class DefaultControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        $this->assertTrue($client->getResponse()->isSuccessful());
        $this->assertRegExp("/Welcome/i", $crawler->filter('h1')->text());
        $this->assertTrue($crawler->filter('p')->count() > 0);
        //nav bar
        $this->assertTrue($crawler->filter('.navbar')->count() > 0);
        $this->assertEquals(3, $crawler->filter('.navbar > li')->count());
        $this->assertRegExp("/home/i", $crawler->filter('.navbar > li:nth-child(1)')->text());
        $this->assertRegExp("/books/i", $crawler->filter('.navbar > li:nth-child(2)')->text());
        $this->assertRegExp("/insert book/i", $crawler->filter('.navbar > li:nth-child(3)')->text());
    }

    public function testBooks()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/books');
        $this->assertTrue($client->getResponse()->isSuccessful());
        $this->assertTrue($crawler->filter(".book-list")->count() > 0);
        $this->assertEquals(3, $crawler->filter(".book-list > tbody > tr")->count());
        $this->assertEquals(3, $crawler->filter(".book-list > tbody > tr:nth-child(1) > td")->count());
        $this->assertRegExp("/Domain-Driven Design/i", $crawler->filter('.book-list > tbody > tr:nth-child(1) > td:nth-child(1)')->text());
        $this->assertRegExp("/Extreme Programming Explained/i", $crawler->filter('.book-list > tbody > tr:nth-child(2) > td:nth-child(1)')->text());
        $this->assertRegExp("/The Clean Coder/i", $crawler->filter('.book-list > tbody > tr:nth-child(3) > td:nth-child(1)')->text());

        $nbQuery = $client->getProfile()->getCollector('db')->getQueryCount();
        $this->assertEquals(1, $nbQuery);
    }

    public function testBookDetailOk()
    {
        $client  = static::createClient();
        $crawler = $client->request('GET', '/books');
        $link    = $crawler->filter('.book-list > tbody > tr:nth-child(1) > td:nth-child(1) > a')->link();
        $crawler = $client->click($link);
        $this->assertTrue($client->getResponse()->isSuccessful());
        $this->assertRegExp("/Eric Evans/i", $crawler->filter('#book-detail')->text());
        $this->assertRegExp("/Domain-Driven Design/i", $crawler->filter('#book-detail')->text());
        $this->assertRegExp("/30\/08\/2003/i", $crawler->filter('#book-detail')->text());
    }

    public function testBookDetail404()
    {
        $client  = static::createClient();
        $crawler = $client->request('GET', '/books/unexistent');
        $this->assertTrue($client->getResponse()->isNotFound());
    }

    public function testCreate_401()
    {
        $client = static::createClient(array(), array(
            'PHP_AUTH_USER' => 'incorrect',
            'PHP_AUTH_PW'   => 'credentials',
        ));

        $crawler = $client->request('GET', '/books/create');

        $this->assertEquals(401, $client->getResponse()->getStatusCode());
    }

    public function testCreate()
    {
        $client = static::createClient(array(), array(
            'PHP_AUTH_USER' => 'user1',
            'PHP_AUTH_PW'   => '1234',
        ));
        $em = static::$kernel->getContainer()->get('doctrine')->getManager();

        $crawler = $client->request('GET', '/books/create');

        $this->assertTrue($client->getResponse()->isSuccessful());
        $this->assertRegExp("/Create Book/i", $crawler->filter('h1')->text());
        $this->assertTrue($crawler->filter('form')->count() == 1);
        $this->assertTrue($crawler->filter('input[name="book[title]"]')->count() == 1);
        $this->assertTrue($crawler->filter('select[name="book[author]"]')->count() == 1);
        $this->assertTrue($crawler->filter('input[name="book[publicationDate]"]')->count() == 1);

        $authorBeck = $em->getRepository('PUGXBookBundle:Author')->findOneBySurname("Beck");
        $form = $crawler->filter('input[type="submit"]')->form();
        $form['book[title]'] = 'Test Driven Development: By Example';
        $form['book[author]']->select($authorBeck->getId());
        $form['book[publicationDate]'] = '18-11-2002';

        $crawler = $client->submit($form);
        $this->assertEquals(302, $client->getResponse()->getStatusCode());

        //mail check
        $mailCollector = $client->getProfile()->getCollector('swiftmailer');
        // Check that an e-mail was sent
        $this->assertEquals(1, $mailCollector->getMessageCount());
        $collectedMessages = $mailCollector->getMessages();
        $message = $collectedMessages[0];
        // Asserting e-mail data
        $this->assertInstanceOf('Swift_Message', $message);
        $this->assertEquals('New Book - Test Driven Development: By Example', $message->getSubject());
        $this->assertEquals('server@pugx-book.org', key($message->getFrom()));
        $this->assertEquals('admin@pugx-book.org', key($message->getTo()));
        $this->assertRegExp('/Test Driven Development: By Example/i',$message->getBody());
        $this->assertRegExp('/Kent Beck/i',$message->getBody());
        $this->assertRegExp('/18-11-2002/i',$message->getBody());
        
        $crawler = $client->followRedirect();
        $this->assertTrue($client->getResponse()->isSuccessful());
        $this->assertTrue($crawler->filter('.notice')->count() == 1);

        //db check
        $book = $em->getRepository('PUGXBookBundle:Book')->findOneBy(array(
            'title'             => "Test Driven Development: By Example",
            'publicationDate'   => new \DateTime('2002-11-18'),
        ));
        $this->assertInstanceOf('PUGX\BookBundle\Entity\Book', $book);
        $this->assertEquals('Beck', $book->getAuthor()->getSurname());
    }

    public function testDelete_401()
    {
        $em = static::$kernel->getContainer()->get('doctrine')->getManager();
        $book = $em->getRepository('PUGXBookBundle:Book')->findOneByTitle('The Clean Coder');

        $client = static::createClient(array(), array(
            'PHP_AUTH_USER' => 'incorrect',
            'PHP_AUTH_PW'   => 'credentials',
        ));

        $crawler = $client->request('GET', '/books/delete/' . $book->getId());

        $this->assertEquals(401, $client->getResponse()->getStatusCode());
    }

    public function testDelete_403()
    {
        $em = static::$kernel->getContainer()->get('doctrine')->getManager();
        $book = $em->getRepository('PUGXBookBundle:Book')->findOneByTitle('The Clean Coder');

        $client = static::createClient(array(), array(
            'PHP_AUTH_USER' => 'user1',
            'PHP_AUTH_PW'   => '1234',
        ));

        $crawler = $client->request('GET', '/books/delete/' . $book->getId());

        $this->assertEquals(403, $client->getResponse()->getStatusCode());
    }

    public function testDelete_404()
    {
        $client = static::createClient(array(), array(
            'PHP_AUTH_USER' => 'admin',
            'PHP_AUTH_PW'   => 'admin',
        ));

        $crawler = $client->request('GET', '/books/delete/non-existing');

        $this->assertEquals(404, $client->getResponse()->getStatusCode());
    }

    public function testDelete()
    {
        $em = static::$kernel->getContainer()->get('doctrine')->getManager();
        $book = $em->getRepository('PUGXBookBundle:Book')->findOneByTitle('The Clean Coder');

        $client = static::createClient(array(), array(
            'PHP_AUTH_USER' => 'admin',
            'PHP_AUTH_PW'   => 'admin',
        ));

        $crawler = $client->request('GET', '/books/delete/' . $book->getId());

        $this->assertEquals(302, $client->getResponse()->getStatusCode());
        $crawler = $client->followRedirect();
        $this->assertTrue($client->getResponse()->isSuccessful());
        $this->assertTrue($crawler->filter('.notice')->count() == 1);

        $book = $em->getRepository('PUGXBookBundle:Book')->findOneByTitle("The Clean Coder");
        $this->assertNull($book);
    }

    public function tearDown()
    {
        $em = static::$kernel->getContainer()->get('doctrine')->getManager();
        $book = $em->getRepository('PUGXBookBundle:Book')->findOneBy(array(
            'title'             => "Test Driven Development: By Example",
        ));
        if ($book) {
            $em->remove($book);
        }

        $book = $em->getRepository('PUGXBookBundle:Book')->findOneBy(array(
            'title' => "The Clean Coder",
        ));
        if (!$book) {
            $martin = $em->getRepository('PUGXBookBundle:Author')->findOneBy(array(
                'surname' => "Martin",
            ));
            $book = new Book(
                "The Clean Coder",
                $martin,
                new \DateTime('2011-5-23')
            );

            $em->persist($book);
        }

        $em->flush();
    }
}
