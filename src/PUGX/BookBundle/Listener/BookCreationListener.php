<?php

namespace PUGX\BookBundle\Listener;

use PUGX\BookBundle\Event\BookCreationEvent;
use PUGX\BookBundle\Service\Mailer;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class BookCreationListener implements EventSubscriberInterface
{
    protected $mailer;
    protected $from;
    protected $to;

    public function __construct(Mailer $mailer, $from, $to)
    {
        $this->mailer = $mailer;
        $this->from   = $from;
        $this->to     = $to;
    }

    public static function getSubscribedEvents()
    {
        return array(
            'pugx.book-creation' => 'notifyBookCreation'
        );
    }

    public function notifyBookCreation(BookCreationEvent $event)
    {
        $this->mailer->sendNewBookNotification($event->getBook(), $this->from, $this->to);
    }
}