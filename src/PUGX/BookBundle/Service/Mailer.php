<?php

namespace PUGX\BookBundle\Service;

use PUGX\BookBundle\Entity\Book;

class Mailer {

    protected $mailer;

    /**
     * @param \Swift_Mailer $mailer
     */
    public function __construct(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public function sendNewBookNotification(Book $book, $from, $to)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject('New Book - ' . $book->getTitle())
            ->setFrom($from)
            ->setTo($to)
            ->setBody('
                        A new book has been inserted:
                        author: ' . $book->getAuthor() . '
                        title: ' . $book->getTitle() . '
                        publication date: ' . $book->getPublicationDate()->format('d-m-Y') . '
                    ')
        ;

        $this->mailer->send($message);
    }
}