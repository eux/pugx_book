<?php

namespace PUGX\BookBundle\Event;

use PUGX\BookBundle\Entity\Book;
use Symfony\Component\EventDispatcher\Event;

class BookCreationEvent extends Event
{
    private $book;

    public function __construct(Book $book)
    {
        $this->book = $book;
    }

    public function getBook()
    {
        return $this->book;
    }
}