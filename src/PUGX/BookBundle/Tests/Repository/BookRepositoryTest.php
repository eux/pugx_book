<?php
namespace PUGX\BookBundle\Tests\Repository;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class BookRepositoryTest extends WebTestCase
{
    private $em;

    private $repo;

    public function setUp()
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();
        $this->em = static::$kernel->getContainer()
            ->get('doctrine')
            ->getManager()
        ;

        $this->repo = $this->em->getRepository('PUGXBookBundle:Book');
    }

    public function testFindAllWithAuthors()
    {
        $result = $this->repo->findAllWithAuthors();

        $this->assertCount(3, $result);
        $firstResult = $result[0];
        $this->assertEquals("Domain-Driven Design: Tackling Complexity in the Heart of Software", $firstResult->getTitle());
    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        parent::tearDown();
        $this->em->close();
    }
}