<?php

namespace PUGX\BookBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BookType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title')
            ->add('author')
            ->add('publicationDate', null, array(
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
            ))
        ;
    }

    public function getName()
    {
        return 'book';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PUGX\BookBundle\Entity\Book',
        ));
    }

}
